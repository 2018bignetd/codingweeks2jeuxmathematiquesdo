from pytest import *
from Coloriage_magique.traitement_image import *

def test_moyenne_color():
    assert moyenne_color([[[100,100,100],[12,18,156]],[[256,256,256],[0,0,0]]]) == [92,93.5,128]
