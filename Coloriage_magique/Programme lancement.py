from Coloriage_magique.coloriage import *

def play():
    #Lance la fenêtre de configuration du jeu
    menu = Tk()
    menu.title('Coloriage magique')
    menu.focus_force()
    game_label = Label(menu, text = "Configuration :")
    game_label.pack()
    frame_play = Frame(menu)
    play_theme = Frame(frame_play)
    play_theme.pack(side = LEFT, padx = 5)
    play_image = Frame(frame_play)
    play_image.pack(side = RIGHT, padx = 5)
    frame_play.pack()
    menu.bind('<Escape>', lambda event : menu.destroy())
    menu.bind('<Return>', lambda event : play_button_script(menu, taille_input.get(),choix_niveau_input.get(ACTIVE), int(difficulte_input.index(ACTIVE)), [int(plus_checkbutton_state.get()), int(minus_checkbutton_state.get()), int(multiplication_checkbutton_state.get())]))
    taille_label = Label(play_image, text = 'Taille de la grille')
    taille_label.pack()
    taille_input = Spinbox(play_image, from_ = 4, to = 100, increment = 1)
    taille_input.pack()
    choix_niveau_label = Label(play_theme, text = 'Choix du niveaux : ')
    choix_niveau_label.pack()
    choix_frame = Frame(play_theme)
    yDefilB = Scrollbar(choix_frame, orient='vertical')
    yDefilB.grid(row=0, column=1, sticky='ns')
    choix_niveau_input = Listbox(choix_frame, yscrollcommand=yDefilB.set, height = 3)
    for key in dico_theme:
        choix_niveau_input.insert(END, key)
    choix_niveau_input.grid(row=0, column=0, sticky='nsew')
    yDefilB['command'] = choix_niveau_input.yview
    choix_frame.pack()
    niveau_selection = Label(play_theme, text = "Niveau sélectionné : " + str(choix_niveau_input.get(ACTIVE)))
    niveau_selection.pack()
    button_image = Button(play_image, text = 'Select image and play', command = lambda : play_image_script(menu, int(taille_input.get()), int(difficulte_input.index(ACTIVE)), [int(plus_checkbutton_state.get()), int(minus_checkbutton_state.get()), int(multiplication_checkbutton_state.get())]))
    button_image.pack()
    difficulte_label = Label(menu, text = 'Difficulté :')
    difficulte_label.pack()
    difficulte_input = Listbox(menu, height = 3)
    difficulte_input.insert(END, "Facile")
    difficulte_input.insert(END, "Moyen")
    difficulte_input.insert(END, "Difficile")
    difficulte_input.pack()
    difficulte_selection = Label(menu, text = "Difficulté choisie :" + str(difficulte_input.get(ACTIVE)))
    difficulte_selection.pack()
    option_frame = Frame(menu)
    option_label = Label(option_frame, text = 'Options :')
    option_label.pack(side = LEFT)
    option_checkbutton_frame = Frame(option_frame)
    plus_checkbutton_state = IntVar()
    minus_checkbutton_state = IntVar()
    multiplication_checkbutton_state = IntVar()
    plus_checkbutton_state.set(1)
    minus_checkbutton_state.set(1)
    multiplication_checkbutton_state.set(1)
    plus_checkbutton = Checkbutton(option_checkbutton_frame, text = "+", variable = plus_checkbutton_state)
    minus_checkbutton = Checkbutton(option_checkbutton_frame, text = "-", variable = minus_checkbutton_state)
    multiplication_checkbutton = Checkbutton(option_checkbutton_frame, text = "*", variable = multiplication_checkbutton_state)
    plus_checkbutton.pack()
    minus_checkbutton.pack()
    multiplication_checkbutton.pack()
    option_checkbutton_frame.pack(side = RIGHT)
    option_frame.pack()
    play_button = Button(play_theme, text = 'Play', command = lambda : play_button_script(menu, taille_input.get(),choix_niveau_input.get(ACTIVE), int(difficulte_input.index(ACTIVE)), [int(plus_checkbutton_state.get()), int(minus_checkbutton_state.get()), int(multiplication_checkbutton_state.get())]))
    play_button.pack()
    while True:
        niveau_selection.config(text = "Niveau sélectionné : " + str(choix_niveau_input.get(ACTIVE)))
        difficulte_selection.config(text = "Difficulté choisie :" + str(difficulte_input.get(ACTIVE)))
        menu.update()


def play_image_script(menu, taille, difficulte, options_choix):
    """Permet de lancer le jeu avec les paramètres définis et une image choisie"""
    try:
        fichier = askopenfilename()
        menu.destroy()
        global options
        options = options_choix
        img = mpimg.imread(fichier)
        img_moyenne = get_color_grid_from_image(img, taille)
        color_matrix = get_color_matrix(img_moyenne, taille)
        game_play(color_matrix, taille, difficulte + 1)
    except:
        print("Select an image")

def play_button_script(menu, taille, niveau, difficulte, options_choix):
    """Permet de lancer le jeu avec les paramètres définis"""
    menu.destroy()
    global options
    options = options_choix
    color_matrix = dico_theme[niveau]
    game_play(color_matrix, taille, difficulte + 1)


play()
