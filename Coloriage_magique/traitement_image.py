import matplotlib.image as mpimg
import numpy as np
import matplotlib.pyplot as plt
from random import random
from math import sqrt
from tkinter.filedialog import askopenfilename
from tkinter import *


def decouper(image, taille_recadre, nombre):
    """Coupe l'image en carré"""
    valeur_particuliere = [int((k * taille_recadre) // nombre) for k in range(nombre + 1)]
    image_decoupe = []
    for i in range(nombre):
        ligne_image_decoupe = []
        for j in range(nombre):
            ligne_image_decoupe.append(image[valeur_particuliere[i]:valeur_particuliere[i + 1],valeur_particuliere[j]:valeur_particuliere[j + 1]])
        image_decoupe.append(ligne_image_decoupe)
    return image_decoupe


def moyenne_color(sous_matrice):
    """Donne la valeur moyenne de la matrice"""
    list_moyenne = [0,0,0]
    for i in range (len(sous_matrice)):
        for j in range (len(sous_matrice[0])):
            for k in range (3):
                list_moyenne[k] += sous_matrice[i][j][k]
    for k in range (3):
        list_moyenne[k] /= (len(sous_matrice)*len(sous_matrice))
    return list_moyenne


def moyenne_matrice(matrice):
    """Donne la matrice moyenne de l'image coupée"""
    matrice_moyenne = []
    for i in range(len(matrice)):
        ligne = []
        for j in range(len(matrice[0])):
            ligne.append(moyenne_color(matrice[i][j]))
        matrice_moyenne.append(ligne)
    return matrice_moyenne


def get_color_grid_from_image(img, taille):
    """Retourne l'image pixelisé avec les couleurs moyennées"""
    nouvelle_image = decouper(img, min(img.shape[0], img.shape[1]), taille)
    return moyenne_matrice(nouvelle_image)


def find_groups(list_colors, nb_group):
    """Permet de trouver des groupes de couleurs proches"""
    list_color_key = [[255 * random() for i in range(3)] for j in range(nb_group)]
    return recursive_find_groups(list_colors, list_color_key, nb_group, 100)


def recursive_find_groups(matrice_image, list_color_key, nb_group, nb_iteration, group_color_key = None):
    """Trouve les groupes de couleurs de manières recursive"""
    if nb_iteration >= 1:
        group_color_key = [[] for i in range(nb_group)]
        for i in range(len(matrice_image)):
            for j in range(len(matrice_image[0])):
                couleur = matrice_image[i][j]
                distance_min = distance_couleur(couleur, list_color_key[0])
                indice = 0
                for k in range(len(list_color_key)):
                    if distance_couleur(couleur, list_color_key[k]) < distance_min:
                        distance_min = distance_couleur(couleur, list_color_key[k])
                        indice = k
                group_color_key[indice].append([couleur, [i, j]])
        nouvelle_list_color_key = []
        for indice in range(len(list_color_key)):
            nouvelle_list_color_key.append(get_baricentre(group_color_key[indice]))
        if nouvelle_list_color_key == list_color_key:
            return list_color_key, group_color_key
        else:
            list_color_key = nouvelle_list_color_key
        return recursive_find_groups(matrice_image, list_color_key, nb_group, nb_iteration - 1, group_color_key)
    else:
        return list_color_key, group_color_key


def distance_couleur(couleur, couleur_key):
    """Retourne la distance dans l'espace des couleurs"""
    return sqrt((couleur[0] - couleur_key[0])**2 + (couleur[1] - couleur_key[1])**2 + (couleur[2] - couleur_key[2])**2)


def get_baricentre(color_group):
    """Retourne le baricentre d'un groupe de couleur"""
    baricentre = [0, 0, 0]
    if len(color_group) != 0:
        for couleur in color_group:
            baricentre[0] += couleur[0][0]
            baricentre[1] += couleur[0][1]
            baricentre[2] += couleur[0][2]
        baricentre[0] /= len(color_group)
        baricentre[1] /= len(color_group)
        baricentre[2] /= len(color_group)
    return baricentre


def rgb2hex(couleur):
    """Transcrit rgb to hex"""
    return "#{:02x}{:02x}{:02x}".format(int(255 * couleur[0]), int(255 * couleur[1]), int(255 * couleur[2]))


def get_index_group_color(position, group_color_key):
    """Retourne l'index de group_color_key contenant position"""
    for i in range(len(group_color_key)):
        for k in range(len(group_color_key[i])):
            if position == group_color_key[i][k][1]:
                return i
    return -1


def get_color_matrix(image_moyenne, taille):
    """Donne la color matrice de taille taille à partir de image"""
    color_matrix = [['' for i in range(taille)] for j in range(taille)]
    color_key, group_color_key = find_groups(image_moyenne, 10)
    for i in range(taille):
        for j in range(taille):
            color_matrix[i][j] = rgb2hex(color_key[get_index_group_color([i, j], group_color_key)])
    return color_matrix


def compare():
    """Lance le menu permettant de configurer la comparaison des étapes de transformation d'une image"""
    parametre_affichage = Tk()
    parametre_affichage.title('Compare steps')
    parametre_affichage.focus_force()
    taille_label = Label(parametre_affichage, text = "Please, enter a length")
    taille_label.pack()
    taille_input = Spinbox(parametre_affichage, from_ = 4, to = 100, increment = 1)
    show_button = Button(parametre_affichage, text = "Show", command = lambda : show_steps(parametre_affichage, int(taille_input.get())))
    parametre_affichage.bind('<Return>', lambda event : show_steps(parametre_affichage, int(taille_input.get())))
    taille_input.pack()
    show_button.pack()
    parametre_affichage.update()


def show_steps(affichage, taille):
    """Affiche la comparaison entre les différentes étapes de transformation d'une image"""
    affichage.destroy()
    fichier = askopenfilename()
    img = mpimg.imread(fichier)
    plt.imshow(img)
    image_moyenne = get_color_grid_from_image(img, taille)
    image_quantifie = get_color_matrix(image_moyenne, taille)
    preview = Tk()
    preview.focus_force()
    longueur = 500
    list_frame = []
    l_quantifie = len(image_quantifie)
    l_moyenne = len(image_moyenne)
    for i in range(l_moyenne):
        list_frame_ligne = []
        for j in range(l_moyenne):
            list_frame_ligne.append(Frame(preview, bg = rgb2hex(image_moyenne[i][j]), height = longueur / taille, width = longueur / taille))
            list_frame_ligne[-1].grid(row = i, column = j)
        for j in range(l_moyenne, l_moyenne + l_quantifie):
            list_frame_ligne.append(Frame(preview, bg = image_quantifie[i][j - l_quantifie], height = longueur / taille, width = longueur / taille))
            list_frame_ligne[-1].grid(row = i, column = j)
        list_frame.append(list_frame_ligne)
    preview.update()
