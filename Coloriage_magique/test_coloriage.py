from pytest import *
from Coloriage_magique import coloriage

def test_generate_calcul_addition():
    assert coloriage.generate_calcul_addition(10) in [['0 + 10', 10, False, 0],['1 + 9', 10, False, 0],['2 + 8', 10, False, 0],
                                                      ['3 + 7', 10, False, 0],['4 + 6', 10, False, 0],['5 + 5', 10, False, 0],
                                                      ['6 + 4', 10, False, 0],['7 + 3', 10, False, 0],['8 + 2', 10, False, 0],
                                                      ['9 + 1', 10, False, 0],['10 + 0', 10, False, 0]]
    assert coloriage.generate_calcul_addition(5) in [['0 + 5', 5, False, 0],['1 + 4', 5, False, 0],['2 + 3', 5, False, 0],
                                                      ['3 + 2', 5, False, 0],['4 + 1', 5, False, 0],['5 + 0', 5, False, 0]]
    assert coloriage.generate_calcul_addition(0) in [['0 + 0', 0, False, 0]]

def test_generate_calcul_soustraction():
    assert coloriage.generate_calcul_soustraction(10,1) in [['20 - 10', 10, False, 0],['19 - 9', 10, False, 0],['18 - 8', 10, False, 0],
                                                      ['17 - 7', 10, False, 0],['16 - 6', 10, False, 0], ['15 - 5', 10, False, 0],['14 - 4', 10, False, 0],
                                                      ['13 - 3', 10, False, 0],['12 - 2', 10, False, 0],['11 - 1', 10, False, 0],
                                                      ['10 - 0', 10, False, 0]]

def test_liste_diviseur():
    assert coloriage.liste_diviseur(10) == [1,2,5,10]
    assert coloriage.liste_diviseur(15) == [1,3,5,15]

def test_generate_calcul_multiplication():
    assert coloriage.generate_calcul_multiplication(10) in [['1 x 10',10, False, 0],['2 x 5',10, False, 0],['5 x 2',10, False, 0],
                                                      ['10 x 1',10, False, 0]]
    assert coloriage.generate_calcul_multiplication(4) in [['1 x 4',4, False, 0],['2 x 2',4, False, 0],['4 x 1',4, False, 0]]
    assert coloriage.generate_calcul_multiplication(0) in [['0 x 1',0, False, 0],['0 x 2',0, False, 0],['0 x 3',0, False, 0],
                                                           ['0 x 4',0, False, 0],['0 x 5',0, False, 0],['0 x 6',0, False, 0],
                                                           ['0 x 7',0, False, 0],['0 x 8',0, False, 0],['0 x 9',0, False, 0],
                                                           ['0 x 10',0, False, 0], ['0 x 0',0, False, 0]]

def test_generate_calcul():
    assert type(coloriage.generate_calcul(20, 2, [1,1,0])) == list
    assert len(coloriage.generate_calcul(20, 2, [1,1,0])) == 4
    assert type(coloriage.generate_calcul(12, 1, [0,1,1])) == list
    assert len(coloriage.generate_calcul(12, 1, [0,1,1])) == 4
    assert type(coloriage.generate_calcul(36, 3, [1,1,1])) == list
    assert len(coloriage.generate_calcul(36, 3, [1,1,1])) == 4

def test_generate_color_grid():
    assert type(coloriage.generate_color_grid([["white","red","red","white"], ["red","pink","blue","white"], ["white","blue","yellow","blue"], ["white","brown","blue","brown"]],
                                         {50 : "white", 25 : "red", 46 : "yellow", 18 : "pink", 78 : "blue", 36 : "brown"},2,
                                         [(0,0), (0,1), (0,2), (0,3), (1,0),(1,1), (1,2), (1,3),(2,0), (2,1), (2,2), (2,3),(3,0), (3,1), (3,2), (3,3)])) == list
    assert len(coloriage.generate_color_grid([["white","red","red","white"], ["red","pink","blue","white"], ["white","blue","yellow","blue"], ["white","brown","blue","brown"]],
                                         {50 : "white", 25 : "red", 46 : "yellow", 18 : "pink", 78 : "blue", 36 : "brown"},2,
                                         [(0,0), (0,1), (0,2), (0,3), (1,0),(1,1), (1,2), (1,3),(2,0), (2,1), (2,2), (2,3),(3,0), (3,1), (3,2), (3,3)])) == 4
    assert len(coloriage.generate_color_grid([["white","red","red","white"], ["red","pink","blue","white"], ["white","blue","yellow","blue"], ["white","brown","blue","brown"]],
                                         {50 : "white", 25 : "red", 46 : "yellow", 18 : "pink", 78 : "blue", 36 : "brown"},2,
                                         [(0,0), (0,1), (0,2), (0,3), (1,0),(1,1), (1,2), (1,3),(2,0), (2,1), (2,2), (2,3),(3,0), (3,2), (3,3)])) == 4

def test_get_list_frame_with_label():
    assert coloriage.get_list_frame_with_label([[[0, 0]], [[0, 1], [0, 2]], [[0, 3], [1, 3]], [[1, 0]], [[1, 1]], [[1, 2]], [[2, 0], [3, 0]],[[2, 1]], [[2, 2]], [[2, 3]], [[3, 1]], [[3, 2]], [[3, 3]]]) == [(0, 0), (0, 1), (0, 3), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2), (2, 3), (3, 1), (3, 2), (3, 3)]
    assert coloriage.get_list_frame_with_label([[[0, 0], [1, 0], [2, 0], [3, 0], [4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [3, 4], [2, 4], [1, 4], [0, 4]], [[0, 1]], [[0, 2]], [[0, 3]], [[1, 1], [1, 2], [1, 3], [2, 3]], [[2, 1]], [[2, 2]], [[3, 1], [3, 2]], [[3, 3]]]) == [(4, 2), (0, 1), (0, 2), (0, 3), (1, 2), (2, 1), (2, 2), (3, 1), (3, 3)]

def test_get_frame_with_label():
    assert coloriage.get_frame_with_label([(0,0),(0,1),(0,2)], 0, 1) == (0,1)
    assert coloriage.get_frame_with_label([(0,0),(0,1),(0,2),(1,1),(2,1)], 1, 1) == (1,1)

def test_distance():
    assert coloriage.distance([5,5],[8,8]) == 18**0.5
    assert coloriage.distance([5,5],[0,0]) == 50**0.5

def test_get_list_of_groups():
    assert coloriage.get_list_of_groups([["white","red","red","white"], ["red","pink","blue","white"],
                                         ["white","blue","yellow","blue"], ["white","brown","blue","brown"]]) == [[[0, 0]], [[0, 1], [0, 2]], [[0, 3], [1, 3]], [[1, 0]], [[1, 1]], [[1, 2]], [[2, 0], [3, 0]], [[2, 1]], [[2, 2]], [[2, 3]], [[3, 1]], [[3, 2]], [[3, 3]]]

def test_fill_sub_group():
    list_1,list_2 = [], []
    coloriage.fill_sub_group([["yellow","black"],["black","black"]], 1, 1, [[False,False],[False,False]], list_1) == None
    assert list_1 == [[1, 1], [0, 1], [1, 0]]
    coloriage.fill_sub_group([["yellow","black"],["red","black"]], 1, 1, [[False,False],[False,False]], list_2) == None
    assert list_2 == [[1, 1], [0, 1]]

def test_is_win():
    assert coloriage.is_win([[['63 - 45 ', 18, False, 0], ['6 x 4 ', 24, False, 0], ['82 - 38 ', 44, False, 0], ['79 - 25 ', 54, False, 0]],
                   [['170 - 71 ', 99, False, 0], ['37 + 31 ', 68, False, 0], ['102 - 55 ', 47, False, 0], ['13 + 23 ', 36, False, 0]],
                   [['2 x 7 ', 14, False, 0], ['0 x 3 ', 0, False, 0], ['69 - 59 ', 10, False, 0], ['8 + 59 ', 67, False, 0]],
                   [['7 x 7 ', 49, False, 0], ['0 + 20 ', 20, False, 0], ['3 + 34 ', 37, False, 0], ['4 x 1 ', 4, False, 0]]]) == False
    assert coloriage.is_win([[['63 - 45 ', 18, True, 0], ['6 x 4 ', 24, False, 0], ['82 - 38 ', 44, False, 0], ['79 - 25 ', 54, False, 0]],
                   [['170 - 71 ', 99, False, 0], ['37 + 31 ', 68, True, 0], ['102 - 55 ', 47, False, 0], ['13 + 23 ', 36, True, 0]],
                   [['2 x 7 ', 14, False, 0], ['0 x 3 ', 0, True, 0], ['69 - 59 ', 10, True, 0], ['8 + 59 ', 67, False, 0]],
                   [['7 x 7 ', 49, True, 0], ['0 + 20 ', 20, False, 0], ['3 + 34 ', 37, False, 0], ['4 x 1 ', 4, True, 0]]]) == False
    assert coloriage.is_win([[['63 - 45 ', 18, True, 0], ['6 x 4 ', 24, True, 0], ['82 - 38 ', 44, True, 0], ['79 - 25 ', 54, True, 0]],
                   [['170 - 71 ', 99, True, 0], ['37 + 31 ', 68, True, 0], ['102 - 55 ', 47, True, 0], ['13 + 23 ', 36, True, 0]],
                   [['2 x 7 ', 14, True, 0], ['0 x 3 ', 0, True, 0], ['69 - 59 ', 10, True, 0], ['8 + 59 ', 67, True, 0]],
                   [['7 x 7 ', 49, True, 0], ['0 + 20 ', 20, True, 0], ['3 + 34 ', 37, True, 0], ['4 x 1 ', 4, True, 0]]]) == True

def test_get_index_of_group_frame():
    assert coloriage.get_index_of_group_frame((0,0), [[[1,0],[1,1],[1,2]],[[0,0],[0,1]]]) == 1
    assert coloriage.get_index_of_group_frame((0,2), [[[1,0],[1,1],[1,2]],[[0,0],[0,1]]]) == -1


def test_get_index_text_grid():
    assert coloriage.get_index_text_grid([[0,[1,1],5]], [[1,0],[1,1],[1,2],[0,0],[0,1]]) == 0
    assert coloriage.get_index_text_grid([[0,[1,3],5]], [[1,0],[1,1],[1,2],[0,0],[0,1]]) == -1

def test_get_score_group():
    assert  coloriage.get_score_group(0, 0, [[['63 - 45 ', 18, False, 1], ['6 x 3 ', 18, False, 2]], [['82 - 38 ', 44, False, 0], ['79 - 25 ', 54, False, 0]]], [[False,False], [False,False]]) == 3
    assert  coloriage.get_score_group(0, 0, [[['63 - 45 ', 18, False, 0], ['6 x 3 ', 18, False, 0]], [['82 - 38 ', 44, False, 0], ['79 - 25 ', 54, False, 0]]], [[False,False], [False,False]]) == 0

def test_generate_liste_color():
    assert coloriage.generate_liste_color([["white","red","red","white"], ["red","pink","blue","white"],
                                           ["white","blue","yellow","blue"], ["white","brown","blue","brown"]]) == ["white","red","pink","blue","yellow","brown"]

def test_is_prime():
    assert coloriage.is_prime(1) == False
    assert coloriage.is_prime(5) == True
    assert coloriage.is_prime(10) == False

def test_generate_dico_color():
    assert type(coloriage.generate_dico_color(["white","red","pink","blue","yellow","brown"])) == dict
    assert type(coloriage.generate_dico_color(["white","red","pink","blue","yellow","brown","purple","grey"])) == dict

def test_color_grid_init():
    assert coloriage.color_grid_init(10) == [[['', 0, False] for i in range(10)] for j in range (10)]
    assert coloriage.color_grid_init(5) == [[['', 0, False] for i in range(5)] for j in range (5)]
    assert coloriage.color_grid_init(0) == [[['', 0, False] for i in range(0)] for j in range (0)]

def test_sorted_dico():
    assert coloriage.sorted_dico({50 : "#FF0000", 10 : "#00FF00", 25 : "#0000FF"}) == {10 : "#00FF00", 25 : "#0000FF", 50 : "#FF0000"}
    assert coloriage.sorted_dico({5 : "#FF0000", 10 : "#00FF00", 25 : "#0000FF", 105 : "#FFFF00"}) == {5 : "#FF0000", 10 : "#00FF00", 25 : "#0000FF",  105 : "#FFFF00"}

def test_hex_to_rgb():
    assert coloriage.hex_to_rgb("#FF0000") == (255, 0, 0)
    assert coloriage.hex_to_rgb("#008000") == (0, 128, 0)
    assert coloriage.hex_to_rgb("#800080") == (128, 0, 128)

def test_normaliser_timer():
    assert coloriage.normaliser_timer(5) == "05"
    assert coloriage.normaliser_timer(13) == "13"

def test_get_timer():
    assert coloriage.get_timer(75,10) == "01 : 05"
    assert coloriage.get_timer(15,15) == "00 : 00"
    assert coloriage.get_timer(660,10) == "10 : 50"
