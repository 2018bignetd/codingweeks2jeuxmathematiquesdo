from tkinter import *
import numpy as np
from Coloriage_magique.traitement_image import *
from tkinter.filedialog import askopenfilename
from Coloriage_magique.données import *

selected_color_key = None
options = [1, 1, 1]

dico_score = {0 : '100',1 : '80', 2: '50',3 :'10'}
score = 0
score_max = 0
time_ref = 0
tiles_font = {"Verdana", 1, "bold"}

def generate_calcul_addition(result):
    """Input : le résultat souhaitee
        Output : Renvoie une liste avec [l'addition, le résultat, False],
                    False permet de dire que la case n'a pas ete encore validee par le joueur"""
    if result == 0 :
        return ['{} + {}'.format(0, result), result, False, 0]
    else :
        nb_a = np.random.randint(0, result)
        nb_b = result - nb_a
        return ['{} + {}'.format(nb_a, nb_b), result, False, 0]


def generate_calcul_soustraction(result, difficulte):
    """Input : le résultat souhaitee et la difficulte
        Output : Renvoie une liste avec [la soustraction, le résultat, False],
                    False permet de dire que la case n'a pas ete encore validee par le joueur"""
    nb_a = np.random.randint(result, result + (10 ** difficulte) + 1)
    nb_b = nb_a - result
    return ['{} - {}'.format(nb_a, nb_b), result, False, 0]


def liste_diviseur(result):
     """ Input : le resultat souhaitee
        Output : la liste des diviseurs du resultat par le joueur"""
     diviseur = []
     for k in range (1, result+1):
         if result % k == 0 :
             diviseur.append(k)
     return diviseur


def generate_calcul_multiplication(result):
    """Input : le résultat souhaitee
        Output : Renvoie une liste avec [la multiplication, le résultat, False],
                    False permet de dire que la case n'a pas ete encore validee par le joueur"""
    if result == 0 :
        nb_a = 0
        nb_b = np.random.randint(0,11)
    else :
        diviseur = liste_diviseur(result)
        long_diviseur = len(diviseur)
        nb_a = diviseur[np.random.randint(0,long_diviseur)]
        nb_b = result // nb_a
    return ['{} x {}'.format(nb_a, nb_b), result, False, 0]


def generate_calcul(result, difficulte, options, op_3_nb = False):
    """Retourne une chaine de caratère de manière aléatoire en prenant en compte la difficulté et les options choisies"""
    x = np.random.rand()
    if x < [0.4, 0.3, 0.2][difficulte - 1]:
        return generate_calcul_addition(result) if options[0] == 1 else generate_calcul(result, difficulte, options, True)
    elif x < [0.8, 0.6, 0.4][difficulte - 1]:
        return generate_calcul_soustraction(result, difficulte) if options[1] == 1 and op_3_nb == False else generate_calcul(result, difficulte, options, True)
    elif x < [1, 1, 0.7][difficulte - 1]:
        return generate_calcul_multiplication(result) if options[2] == 1 else generate_calcul(result, difficulte, options, True)
    elif op_3_nb == False :
        y = np.random.rand()
        if y < 0.5 :
            first_part = generate_calcul_addition(result) if options[0] == 1 else generate_calcul(result, difficulte, options, True)
        else :
            first_part = generate_calcul_multiplication(result) if options[2] == 1 else generate_calcul(result, difficulte, options, True)
        z = np.random.rand()
        if z < 0.5 :
            second_part = generate_calcul(int(first_part[0].split(' ')[0]), difficulte, options, True)
            return [ '(' + second_part[0] + ')' + ' ' + first_part[0].split(' ')[1] + ' ' + first_part[0].split(' ')[2], result, False, 0]
        else :
            second_part = generate_calcul(int(first_part[0].split(' ')[-1]), difficulte, options, True)
            return [ first_part[0].split(' ')[0] + ' ' + first_part[0].split(' ')[1] + ' ' + '(' + second_part[0] + ')', result, False, 0]
    else :
        return generate_calcul(result, difficulte, options, True)




def generate_color_grid(color_matrix, dico_color, difficulte, list_frame_with_label):
    """ Input : la taille de la grille et la difficulte
        Output : Renvoie une matrice de la grille ou chaque case est representee par une sous-liste contenant
                    l'operation, le resultat et un booleen indiquant si la case a ete validee par le joueur"""
    global options
    color_grid = []
    taille = len(color_matrix)
    reversed_dico_color = {key : [k for k in dico_color if key in dico_color[k]][0] for key in dico_color.values()}
    for i in range(taille):
        ligne = []
        for j in range(taille):
            if (i, j) in list_frame_with_label:
                ligne.append(generate_calcul(reversed_dico_color[color_matrix[i][j]], difficulte, options))
            else:
                ligne.append(['', reversed_dico_color[color_matrix[i][j]], False, 0])
        color_grid.append(ligne)
    return color_grid


def get_list_frame_with_label(group_list):
    """Retourne la liste des frames avec des labels"""
    list_frame_with_label = []
    for group in group_list:
        i_center, j_center = 0, 0
        for element in group:
            i_center += element[0]
            j_center += element[1]
        i_center = i_center / len(group)
        j_center = j_center / len(group)
        list_frame_with_label.append(get_frame_with_label(group, i_center, j_center))
    return list_frame_with_label


def get_frame_with_label(group, i_center, j_center):
    i, j = group[0]
    distance_min = distance(group[0], [i_center, j_center])
    if len(group) >= 2:
        for frame in group[1:]:
            if distance(frame, [i_center, j_center]) < distance_min:
                distance_min = distance(frame, [i_center, j_center])
                i, j = frame
    return i, j


from math import sqrt


def distance(x, y):
    return sqrt((x[0] - y[0])**2 + (x[1] - y[1])**2)


def get_list_of_groups(color_matrix):
    """Retourne une liste dont les sous-listes sont les listes de position des frames appartenant au même groupe"""
    group_list = [] #[i, j] liste
    etat = [[False for i in range(len(color_matrix))] for j in range(len(color_matrix))]
    for i in range(len(color_matrix)):
        for j in range(len(color_matrix[0])):
            if not etat[i][j]:
                sub_group = []
                fill_sub_group(color_matrix, i, j, etat, sub_group)
                group_list.append(sub_group)
    return group_list


def fill_sub_group(color_matrix, i, j, etat, sub_group):
    """Retourne le groupe lié à la frame i, j"""
    sub_group.append([i, j])
    etat[i][j] = True
    if i >= 1 and not etat[i - 1][j] and color_matrix[i][j] == color_matrix[i - 1][j]:
        fill_sub_group(color_matrix, i - 1, j, etat, sub_group)
    if j >= 1 and not etat[i][j - 1] and color_matrix[i][j] == color_matrix[i][j - 1]:
        fill_sub_group(color_matrix, i, j - 1, etat, sub_group)
    if i < len(color_matrix) - 1 and not etat[i + 1][j] and color_matrix[i][j] == color_matrix[i + 1][j]:
        fill_sub_group(color_matrix, i + 1, j, etat, sub_group)
    if j < len(color_matrix) - 1 and not etat[i][j + 1] and color_matrix[i][j] == color_matrix[i][j + 1]:
        fill_sub_group(color_matrix, i, j + 1, etat, sub_group)


def is_win(color_grid):
    """ Input : la matrice representant la grille
        Output : renvoie un booleen : True --> si le joueur a valide toutes les cases de la grille / False sinon"""
    taille = len(color_grid)
    for i in range (taille):
        for j in range (taille):
            if not color_grid[i][j][2]:
                return False
    return True

from time import *

def grid_init(color_matrix, dico_color, color_grid, size, group_list, list_frame_with_label):
    """Input : taille de la grille
        Output : Renvoie la grille des Frames et la grille des Labels"""
    taille_frame = 700
    mainFrame = Tk()
    mainFrame.title('Game')
    mainFrame.config(bg = '#f7f7f7', cursor = "spraycan")
    mainFrame.focus_force()
    grid_frame = Frame(mainFrame)
    grid_frame.pack(side = LEFT)
    color_frame = Frame(mainFrame, bg = '#f7f7f7')
    color_frame.pack(side = RIGHT)
    information_frame = Frame(mainFrame, bg = '#f7f7f7')
    information_frame.pack(side = BOTTOM)
    score_time_frame = Frame(information_frame, bg = '#f7f7f7')
    score_label = Label(score_time_frame, text = 'Score : 0', bg = '#f7f7f7')
    score_label.pack()
    timer_label = Label(score_time_frame, text = '00 : 00', bg = '#f7f7f7')
    timer_label.pack()
    score_time_frame.pack(side = BOTTOM)
    generate_legende(color_frame, dico_color)
    graphical_grid = []

    delimiter_horizontaux = []
    delimiter_verticaux = []
    delimiter_center = []
    text_grid = []

    for i in range(size):
        graphical_grid_line = []
        delimiter_verticaux_line = []
        delimiter_horizontaux_line = []
        delimiter_center_line = []
        for j in range(size):
            if j < size - 1:
                delimiter_verticaux_line.append([Frame(grid_frame, width = 5, height = taille_frame / size), ''])
                delimiter_verticaux_line[-1][0].grid(row = 2 * i, column = 2 * j + 1)
            if i < size - 1:
                delimiter_horizontaux_line.append([Frame(grid_frame, height = 5, width = taille_frame / size), ''])
                delimiter_horizontaux_line[-1][0].grid(row = 2 * i + 1, column = 2 * j)
            if j < size - 1 and i < size - 1:
                delimiter_center_line.append(Frame(grid_frame, height = 5, width = 5))
                delimiter_center_line[-1].grid(row = 2 * i + 1, column = 2 * j + 1)
            graphical_grid_line.append(Frame(grid_frame, width = taille_frame / size, height = taille_frame / size))
            graphical_grid_line[-1].grid(row = 2 * i, column = 2 * j)
            graphical_grid_line[-1].bind('<Button-1>', lambda event : click_button(mainFrame, color_grid, graphical_grid, text_grid, dico_color, score_label, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list, event))
            graphical_grid_line[-1].pack_propagate(0)
            if (i, j) in list_frame_with_label:
                text_grid.append([Label(graphical_grid_line[-1], anchor = 'center', height = 1, width = 20, font = tiles_font), (i, j)])
                text_grid[-1][0].place(x = (graphical_grid_line[-1].winfo_reqwidth() - text_grid[-1][0].winfo_reqwidth())/2, y = (graphical_grid_line[-1].winfo_reqheight() - text_grid[-1][0].winfo_reqheight())/2)
        delimiter_verticaux.append(delimiter_verticaux_line)
        delimiter_horizontaux.append(delimiter_horizontaux_line)
        if len(delimiter_center_line) != 0:
            delimiter_center.append(delimiter_center_line)
        graphical_grid.append(graphical_grid_line)
    color_border(color_grid, delimiter_horizontaux, delimiter_verticaux)
    color_center(delimiter_horizontaux, delimiter_verticaux, delimiter_center)
    return mainFrame, graphical_grid, text_grid, timer_label


def update_text_grid(mainFrame, color_grid, text_grid):
    """Affiche les calculs sur la grille"""
    for text_label in text_grid:
        i, j = text_label[1]
        text_label[0].config(text = color_grid[i][j][0], anchor = 'center', font = tiles_font)
    mainFrame.update()


def get_index_of_group_frame(frame, group_list):
    """Retourne l'index du groupe de position contenant la position selectionnée"""
    for i in range(len(group_list)):
        if ([frame[0], frame[1]] in group_list[i]):
            return i
    return -1


def get_index_text_grid(text_grid, group):
    """Renvoie l'index correspondant du label appartenant au groupe sur lequel nous avons cliqué dessus"""
    for i in range(len(text_grid)):
        if [text_grid[i][1][0], text_grid[i][1][1]] in group:
            return i
    return -1


def color_case(i, j, graphical_grid, text_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list):
    """Colorie la case i, j en color et enlève l'équation"""
    graphical_grid[i // 2][j // 2].config(bg = color)
    index_group_list = get_index_of_group_frame((i // 2, j // 2), group_list)
    index_text_grid = get_index_text_grid(text_grid, group_list[index_group_list])
    text_grid[index_text_grid][0].config(bg = color, text = "")
    color_grid[i // 2][j // 2][2] = True
    propagate_color(i, j, graphical_grid, text_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list)
    propagate_border(i, j, graphical_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux)
    propagate_center(i, j, graphical_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center)


def color_border(color_grid, delimiter_horizontaux, delimiter_verticaux):
    """Colorie les bordures noires"""
    for i in range(len(color_grid)):
        for j in range(len(color_grid[0])):
            if j < len(color_grid) - 1 and color_grid[i][j][1] != color_grid[i][j + 1][1]:
                delimiter_verticaux[i][j][0].config(bg = 'black')
                delimiter_verticaux[i][j][1] = "black"
            if i < len(color_grid) - 1 and color_grid[i][j][1] != color_grid[i + 1][j][1]:
                delimiter_horizontaux[i][j][0].config(bg = 'black')
                delimiter_horizontaux[i][j][1] = "black"


def color_center(delimiter_horizontaux, delimiter_verticaux, delimiter_center):
    """Colorie les centres des bordures"""
    for i in range(len(delimiter_center)):
        for j in range(len(delimiter_center[0])):
            if delimiter_verticaux[i][j][1] == "black" or delimiter_verticaux[i + 1][j][1] == "black" or delimiter_horizontaux[i][j][1] == "black" or delimiter_horizontaux[i][j + 1][1] == "black":
                delimiter_center[i][j].config(bg = "black")


def propagate_color(i, j, graphical_grid, text_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list):
    """Propage la couleur pour les Frames de manière récursive"""
    if i // 2 + 1 < len(graphical_grid) and not color_grid[i // 2 + 1][j // 2][2] and color_grid[i // 2][j // 2][1] == color_grid[i // 2 + 1][j // 2][1]:
        color_case(i + 1, j, graphical_grid, text_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list)
    if i // 2 >= 1 and not color_grid[i // 2 - 1][j // 2][2] and color_grid[i // 2][j // 2][1] == color_grid[i // 2 - 1][j // 2][1]:
        color_case(i - 1, j, graphical_grid, text_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list)
    if j // 2 + 1 < len(graphical_grid) and not color_grid[i // 2][j // 2 + 1][2] and color_grid[i // 2][j // 2][1] == color_grid[i // 2][j // 2 + 1][1]:
        color_case(i, j + 1, graphical_grid, text_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list)
    if j // 2 >= 1 and not color_grid[i // 2][j // 2 - 1][2] and color_grid[i // 2][j // 2][1] == color_grid[i // 2][j // 2 - 1][1]:
        color_case(i, j - 1, graphical_grid, text_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list)


def propagate_border(i, j, graphical_grid, color_grid, color, delimiter_horizontaux, delimiter_verticaux):
    """Propage la couleur des délimiteurs"""
    if i // 2 + 1 < len(graphical_grid) and delimiter_horizontaux[i // 2][j // 2][1] != 'black' and color_grid[i // 2][j // 2][1] == color_grid[i // 2 + 1][j // 2][1]:
        delimiter_horizontaux[i // 2][j // 2][0].config(bg = color)
        delimiter_horizontaux[i // 2][j // 2][1] = "colored"
    if i // 2 >= 1 and delimiter_horizontaux[i // 2 - 1][j // 2][1] != 'black' and color_grid[i // 2][j // 2][1] == color_grid[i // 2 - 1][j // 2][1]:
        delimiter_horizontaux[i // 2 - 1][j // 2][0].config(bg = color)
        delimiter_horizontaux[i // 2 - 1][j // 2][1] = "colored"
    if j // 2 + 1 < len(graphical_grid) and delimiter_verticaux[i // 2][j // 2][1] != 'black' and color_grid[i // 2][j // 2][1] == color_grid[i // 2][j // 2 + 1][1]:
        delimiter_verticaux[i // 2][j // 2][0].config(bg = color)
        delimiter_verticaux[i // 2][j // 2][1] = "colored"
    if j // 2 >= 1 and delimiter_verticaux[i // 2][j // 2 - 1][1] != 'black' and color_grid[i // 2][j // 2][1] == color_grid[i // 2][j // 2 - 1][1]:
        delimiter_verticaux[i // 2][j // 2 - 1][0].config(bg = color)
        delimiter_verticaux[i // 2][j // 2 - 1][1] = "colored"


def propagate_center(i, j, graphical_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center):
    """Propagation de la couleur du centre"""
    check_center(i // 2 - 1, j // 2, graphical_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center)
    check_center(i // 2, j // 2, graphical_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center)
    check_center(i // 2 - 1, j // 2 - 1, graphical_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center)
    check_center(i // 2, j // 2 - 1, graphical_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiter_center)


def check_center(i, j, graphical_grid, color, delimiter_horizontaux, delimiter_verticaux, delimiteur_center):
    """Permet de colorier le centre si tous les delimiter sont coloriés"""
    if i >= 0 and i < len(graphical_grid) - 1 and j >= 0 and j < len(graphical_grid) - 1 and delimiter_verticaux[i][j][1] == "colored" and delimiter_verticaux[i + 1][j][1] == "colored" and delimiter_horizontaux[i][j][1] == "colored" and delimiter_horizontaux[i][j + 1][1] == "colored":
        delimiteur_center[i][j].config(bg = color)


def get_score_group(i, j, color_grid, etat):
    """Donne le nombre d'erreur du groupe de pixel à partir de la case i,j"""
    if not etat[i][j]:
        nb_erreur_group = color_grid[i][j][3]
        etat[i][j] = True
        nb_erreur_group += get_score_group(i - 1, j, color_grid, etat) if i >= 1 and color_grid[i][j][1] == color_grid[i - 1][j][1] else 0
        nb_erreur_group += get_score_group(i + 1, j, color_grid, etat) if i < len(color_grid) - 1 and color_grid[i][j][1] == color_grid[i + 1][j][1] else 0
        nb_erreur_group += get_score_group(i, j - 1, color_grid, etat) if j >= 1 and color_grid[i][j][1] == color_grid[i][j - 1][1] else 0
        nb_erreur_group += get_score_group(i, j + 1, color_grid, etat) if j < len(color_grid) - 1 and color_grid[i][j][1] == color_grid[i][j + 1][1] else 0
        return nb_erreur_group
    else:
        return 0


def click_button(mainFrame, color_grid, graphical_grid, text_grid, dico_color, score_label, delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list, event):
    """Fonction si on clique sur la souris. Permet de colorier la case si les conditions sont vérifiées"""
    global selected_color_key
    global score
    global score_max
    global timer_ref
    i = event.widget.grid_info()['row']
    j = event.widget.grid_info()['column']
    if selected_color_key != None and not color_grid[i // 2][j // 2][2]:
        if color_grid[i // 2][j // 2][1] == selected_color_key:
            color_case(i, j, graphical_grid, text_grid, color_grid, dico_color[selected_color_key], delimiter_horizontaux, delimiter_verticaux, delimiter_center, group_list)
            nb_erreur_group = get_score_group(i // 2, j // 2, color_grid, [[False for i in range(len(color_grid))] for j in range(len(color_grid))])
            if nb_erreur_group <= 3 :
                score += int(dico_score[nb_erreur_group])
            score_max += 100
            score_label.config(text='Score : ' + str(score))
            if is_win(color_grid):
                win_fenetre = Tk()
                win_fenetre.bind('<Return>', lambda event : restart_function())
                win_fenetre.focus_force()
                win_message = Label(win_fenetre, text = 'Bravo ! Tu as gagné')
                score_message = Label(win_fenetre,text='Score : '+str(score) + '\n Pourcentage de réussite : ' + str(int((score/score_max)*100))+'%')
                timer_message = Label(win_fenetre, text = "Temps : " + get_timer(time(), time_ref))
                win_message.pack()
                score_message.pack()
                timer_message.pack()
                def restart_function():
                    win_fenetre.destroy()
                    mainFrame.destroy()
                    play()
                restart_button = Button(win_fenetre, text = 'Restart', command = restart_function)
                restart_button.pack()
        else:
            error_frame = Tk()
            error_frame.bind("<Return>", lambda event : error_frame.destroy())
            error_frame.focus_force()
            error_label = Label(error_frame, text = 'Raté, essaye encore !')
            error_button = Button(error_frame, text = "Continuer", command = lambda : error_frame.destroy())
            error_label.pack()
            error_button.pack()
            color_grid [i // 2][j // 2][3] += 1


def generate_liste_color(color_matrix):
    """Genère la liste des couleurs contenues dans la color_matrix"""
    liste_color = []
    for i in range(len(color_matrix)):
        for j in range(len(color_matrix[0])):
            if color_matrix[i][j] not in liste_color:
                liste_color.append(color_matrix[i][j])
    return liste_color


def is_prime(n):
    """Input : Nombre entier
    Output : Renvoie True si le nombre est premier / False sinon"""
    if n == 0 or n == 1:
        return False
    for i in range (2,n):
        if n % i == 0:
            return False
    return True


def generate_dico_color(liste_color):
    """Input : Liste des differentes couleurs
    Output : Renvoie un dictionnaire avec un resultat different par couleur"""
    dico = {}
    liste_indices = []
    count = 0
    while count < len(liste_color):
        indice = np.random.randint(0,100)
        if indice not in liste_indices and not is_prime(indice):
            dico[indice] = liste_color[count]
            liste_indices.append(indice)
            count += 1
    return sorted_dico(dico)


def color_grid_init(taille):
    """Input : taille de la grille
    Output : Renvoie la grille initialisee """
    return [[['', 0, False] for i in range(taille)] for j in range (taille)]


def sorted_dico(dico_color):
    """Retourne le dictionnaire trié par clef"""
    key_sorted = sorted(list(dico_color))
    return {key : dico_color[key] for key in key_sorted}


def generate_legende(color_frame, dico_color):
    """Affiche la légende sur la droite de l'affichage"""
    color_frame.pack_propagate(0)
    color_legende = Frame(color_frame, bg = '#f7f7f7')
    color_legende.grid(row = 0)
    color_legende_line = []
    i = 0
    for key in dico_color.keys():
        color_line = [Label(color_legende, text = str(key), bg = '#f7f7f7')]
        color_line[0].grid(row = i, column = 0)
        color_line.append(Frame(color_legende, bg = dico_color[int(key)], height = 50, width = 50))
        color_line[1].grid(row = i, column = 1)
        color_line[1].bind('<Button-1>', lambda event : pick_up_color(event, dico_color, selected_color))
        color_legende_line.append(color_line)
        i += 1
    selected_color_label = Label(color_frame, text = "\n \n Couleur selectionnée :", bg = '#f7f7f7')
    selected_color_label.grid(row = 1)
    selected_color = Frame(color_frame, width = 50, height = 50, bg = 'black')
    selected_color.grid(row = 2)


def pick_up_color(event, dico_color, selected_color):
    """Récupère la couleur selectionnée quand on clique avec la souris sur une case de la légende"""
    global selected_color_key
    numero_ligne = event.widget.grid_info()['row']
    selected_color_key = [key for key in dico_color.keys()][numero_ligne]
    selected_color.config(bg = dico_color[selected_color_key])


def normaliser_timer(time_to_show):
    """Permet d'obtenir deux chiffres dans tous les cas"""
    return '0' + str(time_to_show) if len(str(time_to_show)) == 1 else str(time_to_show)


def get_timer(instant_t, time_ref):
    """Retourne sous forme de caractère le timer"""
    time_to_show = int(instant_t - time_ref)
    return normaliser_timer(time_to_show // 60) + " : " + normaliser_timer(time_to_show % 60)


def game_play(color_matrix, taille, difficulte):
    """Lancement du jeu"""
    global time_ref
    size = len(color_matrix)
    time_ref = time()
    last_time = time_ref
    group_list = get_list_of_groups(color_matrix)
    list_frame_with_label = get_list_frame_with_label(group_list)
    dico_color = generate_dico_color(generate_liste_color(color_matrix))
    color_grid = generate_color_grid(color_matrix, dico_color, difficulte, list_frame_with_label)
    mainFrame, graphical_grid, text_grid, timer_label = grid_init(color_matrix, dico_color, color_grid, size, group_list, list_frame_with_label)
    update_text_grid(mainFrame, color_grid, text_grid)
    while True:
        instant_t = time()
        if int(instant_t - last_time) >= 1:
            last_time = instant_t
            timer_label.config(text = get_timer(instant_t, time_ref))
        mainFrame.update()
    return 0
